
def package_do_shlibs(d):
    import re, pipes

    exclude_shlibs = d.getVar('EXCLUDE_FROM_SHLIBS', 0)
    if exclude_shlibs:
        bb.note("not generating shlibs")
        return

    lib_re = re.compile("^.*\.so")
    libdir_re = re.compile(".*/%s$" % d.getVar('baselib', True))

    packages = d.getVar('PACKAGES', True)
    targetos = d.getVar('TARGET_OS', True)

    workdir = d.getVar('WORKDIR', True)

    ver = d.getVar('PKGV', True)
    if not ver:
        msg = "PKGV not defined"
        package_qa_handle_error("pkgv-undefined", msg, d)
        return

    pkgdest = d.getVar('PKGDEST', True)

    shlibs_dirs = d.getVar('SHLIBSDIRS', True).split()
    shlibswork_dir = d.getVar('SHLIBSWORKDIR', True)

    # Take shared lock since we're only reading, not writing
    lf = bb.utils.lockfile(d.expand("/home/tanmayi/QorIQ-SDK-V1.7-20141218-yocto/poky/build_t1040rdb_release/tmp/sysroots/package-output.lock"))

    def read_shlib_providers():
        list_re = re.compile('^(.*)\.list$')
        # Go from least to most specific since the last one found wins
        for dir in reversed(shlibs_dirs):
            bb.debug(2, "Reading shlib providers in %s" % (dir))
            if not os.path.exists(dir):
                continue
            for file in os.listdir(dir):
                m = list_re.match(file)
                if m:
                    dep_pkg = m.group(1)
                    fd = open(os.path.join(dir, file))
                    lines = fd.readlines()
                    fd.close()
                    ver_file = os.path.join(dir, dep_pkg + '.ver')
                    lib_ver = None
                    if os.path.exists(ver_file):
                        fd = open(ver_file)
                        lib_ver = fd.readline().rstrip()
                        fd.close()
                    for l in lines:
                        shlib_provider[l.rstrip()] = (dep_pkg, lib_ver)

    def linux_so(file):
        needs_ldconfig = False
        cmd = d.getVar('OBJDUMP', True) + " -p " + pipes.quote(file) + " 2>/dev/null"
        fd = os.popen(cmd)
        lines = fd.readlines()
        fd.close()
        for l in lines:
            m = re.match("\s+NEEDED\s+([^\s]*)", l)
            if m:
                if m.group(1) not in needed[pkg]:
                    needed[pkg].append(m.group(1))
                if m.group(1) not in needed_from:
                    needed_from[m.group(1)] = []
                needed_from[m.group(1)].append(file)
            m = re.match("\s+SONAME\s+([^\s]*)", l)
            if m:
                this_soname = m.group(1)
                if not this_soname in sonames:
                    # if library is private (only used by package) then do not build shlib for it
                    if not private_libs or this_soname not in private_libs:
                        sonames.append(this_soname)
                if libdir_re.match(os.path.dirname(file)):
                    needs_ldconfig = True
                if snap_symlinks and (os.path.basename(file) != this_soname):
                    renames.append((file, os.path.join(os.path.dirname(file), this_soname)))
        return needs_ldconfig

    def darwin_so(file):
        if not os.path.exists(file):
            return

        def get_combinations(base):
            #
            # Given a base library name, find all combinations of this split by "." and "-"
            #
            combos = []
            options = base.split(".")
            for i in range(1, len(options) + 1):
                combos.append(".".join(options[0:i]))
            options = base.split("-")
            for i in range(1, len(options) + 1):
                combos.append("-".join(options[0:i]))
            return combos

        if (file.endswith('.dylib') or file.endswith('.so')) and not pkg.endswith('-dev') and not pkg.endswith('-dbg'):
            # Drop suffix
            name = os.path.basename(file).rsplit(".",1)[0]
            # Find all combinations
            combos = get_combinations(name)
            for combo in combos:
                if not combo in sonames:
                    sonames.append(combo)
        if file.endswith('.dylib') or file.endswith('.so'):
            lafile = file.replace(os.path.join(pkgdest, pkg), d.getVar('PKGD', True))
            # Drop suffix
            lafile = lafile.rsplit(".",1)[0]
            lapath = os.path.dirname(lafile)
            lafile = os.path.basename(lafile)
            # Find all combinations
            combos = get_combinations(lafile)
            for combo in combos:
                if os.path.exists(lapath + '/' + combo + '.la'):
                    break
            lafile = lapath + '/' + combo + '.la'

            #bb.note("Foo2: %s" % lafile)
            #bb.note("Foo %s" % file)
            if os.path.exists(lafile):
                fd = open(lafile, 'r')
                lines = fd.readlines()
                fd.close()
                for l in lines:
                    m = re.match("\s*dependency_libs=\s*'(.*)'", l)
                    if m:
                        deps = m.group(1).split(" ")
                        for dep in deps:
                            #bb.note("Trying %s for %s" % (dep, pkg))
                            name = None
                            if dep.endswith(".la"):
                                name = os.path.basename(dep).replace(".la", "")
                            elif dep.startswith("-l"):
                                name = dep.replace("-l", "lib")
                            if pkg not in needed:
                                needed[pkg] = []
                            if name and name not in needed[pkg]:
                                needed[pkg].append(name)
                            if name not in needed_from:
                                needed_from[name] = []
                            if lafile and lafile not in needed_from[name]:
                                needed_from[name].append(lafile)
                                #bb.note("Adding %s for %s" % (name, pkg))

    if d.getVar('PACKAGE_SNAP_LIB_SYMLINKS', True) == "1":
        snap_symlinks = True
    else:
        snap_symlinks = False

    if (d.getVar('USE_LDCONFIG', True) or "1") == "1":
        use_ldconfig = True
    else:
        use_ldconfig = False

    needed = {}
    needed_from = {}
    shlib_provider = {}
    read_shlib_providers()

    for pkg in packages.split():
        private_libs = d.getVar('PRIVATE_LIBS_' + pkg, True) or d.getVar('PRIVATE_LIBS', True) or ""
        private_libs = private_libs.split()
        needs_ldconfig = False
        bb.debug(2, "calculating shlib provides for %s" % pkg)

        pkgver = d.getVar('PKGV_' + pkg, True)
        if not pkgver:
            pkgver = d.getVar('PV_' + pkg, True)
        if not pkgver:
            pkgver = ver

        needed[pkg] = []
        sonames = list()
        renames = list()
        for file in pkgfiles[pkg]:
                soname = None
                if cpath.islink(file):
                    continue
                if targetos == "darwin" or targetos == "darwin8":
                    darwin_so(file)
                elif os.access(file, os.X_OK) or lib_re.match(file):
                    ldconfig = linux_so(file)
                    needs_ldconfig = needs_ldconfig or ldconfig
        for (old, new) in renames:
            bb.note("Renaming %s to %s" % (old, new))
            os.rename(old, new)
            pkgfiles[pkg].remove(old)

        shlibs_file = os.path.join(shlibswork_dir, pkg + ".list")
        shver_file = os.path.join(shlibswork_dir, pkg + ".ver")
        if len(sonames):
            fd = open(shlibs_file, 'w')
            for s in sonames:
                if s in shlib_provider:
                    (old_pkg, old_pkgver) = shlib_provider[s]
                    if old_pkg != pkg:
                        bb.warn('%s-%s was registered as shlib provider for %s, changing it to %s-%s because it was built later' % (old_pkg, old_pkgver, s, pkg, pkgver))
                bb.debug(1, 'registering %s-%s as shlib provider for %s' % (pkg, pkgver, s))
                fd.write(s + '\n')
                shlib_provider[s] = (pkg, pkgver)
            fd.close()
            fd = open(shver_file, 'w')
            fd.write(pkgver + '\n')
            fd.close()
        if needs_ldconfig and use_ldconfig:
            bb.debug(1, 'adding ldconfig call to postinst for %s' % pkg)
            postinst = d.getVar('pkg_postinst_%s' % pkg, True)
            if not postinst:
                postinst = '#!/bin/sh\n'
            postinst += d.getVar('ldconfig_postinst_fragment', True)
            d.setVar('pkg_postinst_%s' % pkg, postinst)
        bb.debug(1, 'LIBNAMES: pkg %s sonames %s' % (pkg, sonames))

    bb.utils.unlockfile(lf)

    assumed_libs = d.getVar('ASSUME_SHLIBS', True)
    if assumed_libs:
        for e in assumed_libs.split():
            l, dep_pkg = e.split(":")
            lib_ver = None
            dep_pkg = dep_pkg.rsplit("_", 1)
            if len(dep_pkg) == 2:
                lib_ver = dep_pkg[1]
            dep_pkg = dep_pkg[0]
            shlib_provider[l] = (dep_pkg, lib_ver)

    for pkg in packages.split():
        bb.debug(2, "calculating shlib requirements for %s" % pkg)

        deps = list()
        for n in needed[pkg]:
            # if n is in private libraries, don't try to search provider for it
            # this could cause problem in case some abc.bb provides private
            # /opt/abc/lib/libfoo.so.1 and contains /usr/bin/abc depending on system library libfoo.so.1
            # but skipping it is still better alternative than providing own
            # version and then adding runtime dependency for the same system library
            if private_libs and n in private_libs:
                bb.debug(2, '%s: Dependency %s covered by PRIVATE_LIBS' % (pkg, n))
                continue
            if n in shlib_provider.keys():
                (dep_pkg, ver_needed) = shlib_provider[n]

                bb.debug(2, '%s: Dependency %s requires package %s (used by files: %s)' % (pkg, n, dep_pkg, needed_from[n]))

                if dep_pkg == pkg:
                    continue

                if ver_needed:
                    dep = "%s (>= %s)" % (dep_pkg, ver_needed)
                else:
                    dep = dep_pkg
                if not dep in deps:
                    deps.append(dep)
            else:
                bb.note("Couldn't find shared library provider for %s, used by files: %s" % (n, needed_from[n]))

        deps_file = os.path.join(pkgdest, pkg + ".shlibdeps")
        if os.path.exists(deps_file):
            os.remove(deps_file)
        if len(deps):
            fd = open(deps_file, 'w')
            for dep in deps:
                fd.write(dep + '\n')
            fd.close()


package_do_shlibs(d)
