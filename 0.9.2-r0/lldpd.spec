Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments
Name: lldpd
Version: 0.9.2
Release: r0
License: ISC
Group: net/misc
Packager: Freescale Yocto Team <yocto@linux.freescale.net>
BuildRequires: pkgconfig-native
BuildRequires: libtool-cross
BuildRequires: autoconf-native
BuildRequires: libtool-native
BuildRequires: base-files
BuildRequires: shadow-native
BuildRequires: virtual/powerpc-fsl-linux-compilerlibs
BuildRequires: update-rc.d-native
BuildRequires: initscripts
BuildRequires: gnu-config-native
BuildRequires: libbsd
BuildRequires: virtual/powerpc-fsl-linux-gcc
BuildRequires: libevent
BuildRequires: shadow-sysroot
BuildRequires: virtual/libc
BuildRequires: shadow
BuildRequires: base-passwd
BuildRequires: automake-native
Requires: libc.so.6(GLIBC_2.2.3)
Requires: libbsd.so.0
Requires: libevent >= 2.0.21
Requires: libc.so.6(GLIBC_2.3)
Requires: liblldpctl.so.4(LIBLLDPCTL_4.6)
Requires: libc.so.6(GLIBC_2.3.4)
Requires: base-files
Requires: libc.so.6(GLIBC_2.1)
Requires: libbsd.so.0(LIBBSD_0.6)
Requires: libc.so.6(GLIBC_2.1.3)
Requires: libpthread.so.0(GLIBC_2.0)
Requires: libevent-2.0.so.5
Requires: libbsd.so.0(LIBBSD_0.0)
Requires: os-release
Requires: libc.so.6(GLIBC_2.8)
Requires: /bin/sh
Requires: libc.so.6
Requires: libc.so.6(GLIBC_2.4)
Requires: libc.so.6(GLIBC_2.0)
Requires: libc.so.6(GLIBC_2.2)
Requires: libpthread.so.0
Requires: libc6 >= 2.15
Requires: shadow
Requires: rtld(GNU_HASH)
Requires: libbsd.so.0(LIBBSD_0.5)
Requires: liblldpctl.so.4
Requires: liblldpctl.so.4(LIBLLDPCTL_4.7)
Requires: liblldpctl.so.4(LIBLLDPCTL_4.8)
Requires: initscripts-functions
Requires: base-passwd
Requires: libbsd0 >= 0.6.0
Requires(pre): libc.so.6(GLIBC_2.2.3)
Requires(pre): libbsd.so.0
Requires(pre): libevent >= 2.0.21
Requires(pre): libc.so.6(GLIBC_2.3)
Requires(pre): liblldpctl.so.4(LIBLLDPCTL_4.6)
Requires(pre): libc.so.6(GLIBC_2.3.4)
Requires(pre): base-files
Requires(pre): libc.so.6(GLIBC_2.1)
Requires(pre): libbsd.so.0(LIBBSD_0.6)
Requires(pre): libc.so.6(GLIBC_2.1.3)
Requires(pre): libpthread.so.0(GLIBC_2.0)
Requires(pre): libevent-2.0.so.5
Requires(pre): libbsd.so.0(LIBBSD_0.0)
Requires(pre): os-release
Requires(pre): libc.so.6(GLIBC_2.8)
Requires(pre): /bin/sh
Requires(pre): libc.so.6
Requires(pre): libc.so.6(GLIBC_2.4)
Requires(pre): libc.so.6(GLIBC_2.0)
Requires(pre): libc.so.6(GLIBC_2.2)
Requires(pre): libpthread.so.0
Requires(pre): libc6 >= 2.15
Requires(pre): shadow
Requires(pre): rtld(GNU_HASH)
Requires(pre): libbsd.so.0(LIBBSD_0.5)
Requires(pre): liblldpctl.so.4
Requires(pre): liblldpctl.so.4(LIBLLDPCTL_4.7)
Requires(pre): liblldpctl.so.4(LIBLLDPCTL_4.8)
Requires(pre): initscripts-functions
Requires(pre): base-passwd
Requires(pre): libbsd0 >= 0.6.0
Requires(post): libc.so.6(GLIBC_2.2.3)
Requires(post): libbsd.so.0
Requires(post): libevent >= 2.0.21
Requires(post): libc.so.6(GLIBC_2.3)
Requires(post): liblldpctl.so.4(LIBLLDPCTL_4.6)
Requires(post): libc.so.6(GLIBC_2.3.4)
Requires(post): base-files
Requires(post): libc.so.6(GLIBC_2.1)
Requires(post): libbsd.so.0(LIBBSD_0.6)
Requires(post): libc.so.6(GLIBC_2.1.3)
Requires(post): libpthread.so.0(GLIBC_2.0)
Requires(post): libevent-2.0.so.5
Requires(post): libbsd.so.0(LIBBSD_0.0)
Requires(post): os-release
Requires(post): libc.so.6(GLIBC_2.8)
Requires(post): /bin/sh
Requires(post): libc.so.6
Requires(post): libc.so.6(GLIBC_2.4)
Requires(post): libc.so.6(GLIBC_2.0)
Requires(post): libc.so.6(GLIBC_2.2)
Requires(post): libpthread.so.0
Requires(post): libc6 >= 2.15
Requires(post): shadow
Requires(post): rtld(GNU_HASH)
Requires(post): libbsd.so.0(LIBBSD_0.5)
Requires(post): liblldpctl.so.4
Requires(post): liblldpctl.so.4(LIBLLDPCTL_4.7)
Requires(post): liblldpctl.so.4(LIBLLDPCTL_4.8)
Requires(post): initscripts-functions
Requires(post): base-passwd
Requires(post): libbsd0 >= 0.6.0
Requires(preun): libc.so.6(GLIBC_2.2.3)
Requires(preun): libbsd.so.0
Requires(preun): libevent >= 2.0.21
Requires(preun): libc.so.6(GLIBC_2.3)
Requires(preun): liblldpctl.so.4(LIBLLDPCTL_4.6)
Requires(preun): libc.so.6(GLIBC_2.3.4)
Requires(preun): base-files
Requires(preun): libc.so.6(GLIBC_2.1)
Requires(preun): libbsd.so.0(LIBBSD_0.6)
Requires(preun): libc.so.6(GLIBC_2.1.3)
Requires(preun): libpthread.so.0(GLIBC_2.0)
Requires(preun): libevent-2.0.so.5
Requires(preun): libbsd.so.0(LIBBSD_0.0)
Requires(preun): os-release
Requires(preun): libc.so.6(GLIBC_2.8)
Requires(preun): /bin/sh
Requires(preun): libc.so.6
Requires(preun): libc.so.6(GLIBC_2.4)
Requires(preun): libc.so.6(GLIBC_2.0)
Requires(preun): libc.so.6(GLIBC_2.2)
Requires(preun): libpthread.so.0
Requires(preun): libc6 >= 2.15
Requires(preun): shadow
Requires(preun): rtld(GNU_HASH)
Requires(preun): libbsd.so.0(LIBBSD_0.5)
Requires(preun): liblldpctl.so.4
Requires(preun): liblldpctl.so.4(LIBLLDPCTL_4.7)
Requires(preun): liblldpctl.so.4(LIBLLDPCTL_4.8)
Requires(preun): initscripts-functions
Requires(preun): base-passwd
Requires(preun): libbsd0 >= 0.6.0
Requires(postun): libc.so.6(GLIBC_2.2.3)
Requires(postun): libbsd.so.0
Requires(postun): libevent >= 2.0.21
Requires(postun): libc.so.6(GLIBC_2.3)
Requires(postun): liblldpctl.so.4(LIBLLDPCTL_4.6)
Requires(postun): libc.so.6(GLIBC_2.3.4)
Requires(postun): base-files
Requires(postun): libc.so.6(GLIBC_2.1)
Requires(postun): libbsd.so.0(LIBBSD_0.6)
Requires(postun): libc.so.6(GLIBC_2.1.3)
Requires(postun): libpthread.so.0(GLIBC_2.0)
Requires(postun): libevent-2.0.so.5
Requires(postun): libbsd.so.0(LIBBSD_0.0)
Requires(postun): os-release
Requires(postun): libc.so.6(GLIBC_2.8)
Requires(postun): /bin/sh
Requires(postun): libc.so.6
Requires(postun): libc.so.6(GLIBC_2.4)
Requires(postun): libc.so.6(GLIBC_2.0)
Requires(postun): libc.so.6(GLIBC_2.2)
Requires(postun): libpthread.so.0
Requires(postun): libc6 >= 2.15
Requires(postun): shadow
Requires(postun): rtld(GNU_HASH)
Requires(postun): libbsd.so.0(LIBBSD_0.5)
Requires(postun): liblldpctl.so.4
Requires(postun): liblldpctl.so.4(LIBLLDPCTL_4.7)
Requires(postun): liblldpctl.so.4(LIBLLDPCTL_4.8)
Requires(postun): initscripts-functions
Requires(postun): base-passwd
Requires(postun): libbsd0 >= 0.6.0
Suggests: update-rc.d
Provides: liblldpctl.so.4(LIBLLDPCTL_4.7)
Provides: liblldpctl.so.4(LIBLLDPCTL_4.8)
Provides: elf(buildid) = 0de2573b14c108801c50fc05cdc8583aad621a93
Provides: elf(buildid) = 61200d8c5cf97c55bdb3f4833c1ec9e457c080ea
Provides: elf(buildid) = 78c717abebd1603c78edcc4772d6c5fe801cd9b5
Provides: liblldpctl.so.4
Provides: liblldpctl.so.4(LIBLLDPCTL_4.6)

%description
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.

%package -n lldpd-zsh-completion
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments
Group: net/misc

%description -n lldpd-zsh-completion
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.

%package -n lldpd-dbg
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments - Debugging files
Group: devel
Suggests: libc6-dbg
Suggests: libbsd-dbg
Suggests: libevent-dbg

%description -n lldpd-dbg
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.  This package contains ELF symbols and related sources for
debugging purposes.

%package -n lldpd-staticdev
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments - Development files (Static Libraries)
Group: devel
Requires: lldpd-dev = 0.9.2-r0

%description -n lldpd-staticdev
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.  This package contains static libraries for software
development.

%package -n lldpd-dev
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments - Development files
Group: devel
Requires: lldpd = 0.9.2-r0
Suggests: base-passwd-dev
Suggests: shadow-dev
Suggests: libevent-dev
Suggests: shadow-sysroot-dev
Suggests: base-files-dev
Suggests: initscripts-functions-dev
Suggests: libbsd-dev
Suggests: libc6-dev
Suggests: bash-completion-dev
Suggests: os-release-dev
Suggests: initscripts-dev

%description -n lldpd-dev
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.  This package contains symbolic links, header files, and
related items necessary for software development.

%package -n lldpd-doc
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments - Documentation files
Group: doc

%description -n lldpd-doc
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.  This package contains documentation.

%package -n lldpd-locale
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments
Group: net/misc

%description -n lldpd-locale
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.

%package -n lldpd-bash-completion
Summary: A 802.1ab implementation (LLDP) to help you locate neighbors of all your equipments
Group: net/misc
Requires: bash-completion

%description -n lldpd-bash-completion
A 802.1ab implementation (LLDP) to help you locate neighbors of all your
equipments.

%pre
# lldpd - preinst
#!/bin/sh
if [ -z "$D" -a -f "/etc/init.d/lldpd" ]; then
	/etc/init.d/lldpd stop
fi
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-f -r $D"
	else
		OPT="-f"
	fi
	update-rc.d $OPT lldpd remove
fi
bbnote () {
	echo "NOTE: $*"
}
bbwarn () {
	echo "WARNING: $*"
}
bbfatal () {
	echo "ERROR: $*"
	exit 1
}
perform_groupadd () {
	local rootdir="$1"
	local opts="$2"
	local retries="$3"
	bbnote "Performing groupadd with [$opts] and $retries times of retry"
	local groupname=`echo "$opts" | awk '{ print $NF }'`
	local group_exists="`grep "^$groupname:" $rootdir/etc/group || true`"
	if test "x$group_exists" = "x"; then
		local count=0
		while true; do
			eval $PSEUDO groupadd $opts || true
			group_exists="`grep "^$groupname:" $rootdir/etc/group || true`"
			if test "x$group_exists" = "x"; then
				bbwarn "groupadd command did not succeed. Retrying..."
			else
				break
			fi
			count=`expr $count + 1`
			if test $count = $retries; then
				bbfatal "Tried running groupadd command $retries times without scucess, giving up"
			fi
                        sleep $count
		done
	else
		bbwarn "group $groupname already exists, not re-creating it"
	fi
}
perform_useradd () {
	local rootdir="$1"
	local opts="$2"
	local retries="$3"
	bbnote "Performing useradd with [$opts] and $retries times of retry"
	local username=`echo "$opts" | awk '{ print $NF }'`
	local user_exists="`grep "^$username:" $rootdir/etc/passwd || true`"
	if test "x$user_exists" = "x"; then
	       local count=0
	       while true; do
		       eval $PSEUDO useradd $opts || true
		       user_exists="`grep "^$username:" $rootdir/etc/passwd || true`"
		       if test "x$user_exists" = "x"; then
			       bbwarn "useradd command did not succeed. Retrying..."
		       else
			       break
		       fi
		       count=`expr $count + 1`
		       if test $count = $retries; then
				bbfatal "Tried running useradd command $retries times without scucess, giving up"
		       fi
		       sleep $count
	       done
	else
		bbwarn "user $username already exists, not re-creating it"
	fi
}
perform_groupmems () {
	local rootdir="$1"
	local opts="$2"
	local retries="$3"
	bbnote "Performing groupmems with [$opts] and $retries times of retry"
	local groupname=`echo "$opts" | awk '{ for (i = 1; i < NF; i++) if ($i == "-g" || $i == "--group") print $(i+1) }'`
	local username=`echo "$opts" | awk '{ for (i = 1; i < NF; i++) if ($i == "-a" || $i == "--add") print $(i+1) }'`
	bbnote "Running groupmems command with group $groupname and user $username"
	# groupmems fails if /etc/gshadow does not exist
	local gshadow=""
	if [ -f $rootdir/etc/gshadow ]; then
		gshadow="yes"
	else
		gshadow="no"
		touch $rootdir/etc/gshadow
	fi
	local mem_exists="`grep "^$groupname:[^:]*:[^:]*:\([^,]*,\)*$username\(,[^,]*\)*" $rootdir/etc/group || true`"
	if test "x$mem_exists" = "x"; then
		local count=0
		while true; do
			eval $PSEUDO groupmems $opts || true
			mem_exists="`grep "^$groupname:[^:]*:[^:]*:\([^,]*,\)*$username\(,[^,]*\)*" $rootdir/etc/group || true`"
			if test "x$mem_exists" = "x"; then
				bbwarn "groupmems command did not succeed. Retrying..."
			else
				break
			fi
			count=`expr $count + 1`
			if test $count = $retries; then
				if test "x$gshadow" = "xno"; then
					rm -f $rootdir/etc/gshadow
					rm -f $rootdir/etc/gshadow-
				fi
				bbfatal "Tried running groupmems command $retries times without scucess, giving up"
			fi
			sleep $count
		done
	else
		bbwarn "group $groupname already contains $username, not re-adding it"
	fi
	if test "x$gshadow" = "xno"; then
		rm -f $rootdir/etc/gshadow
		rm -f $rootdir/etc/gshadow-
	fi
}
OPT=""
SYSROOT=""

if test "x$D" != "x"; then
	# Installing into a sysroot
	SYSROOT="$D"
	OPT="--root $D"
fi

# If we're not doing a special SSTATE/SYSROOT install
# then set the values, otherwise use the environment
if test "x$UA_SYSROOT" = "x"; then
	# Installing onto a target
	# Add groups and users defined only for this package
	GROUPADD_PARAM="--system lldpd"
	USERADD_PARAM="--system -g lldpd --shell /bin/false lldpd"
	GROUPMEMS_PARAM="${GROUPMEMS_PARAM}"
fi

# Perform group additions first, since user additions may depend
# on these groups existing
if test "x$GROUPADD_PARAM" != "x"; then
	echo "Running groupadd commands..."
	# Invoke multiple instances of groupadd for parameter lists
	# separated by ';'
	opts=`echo "$GROUPADD_PARAM" | cut -d ';' -f 1`
	remaining=`echo "$GROUPADD_PARAM" | cut -d ';' -f 2-`
	while test "x$opts" != "x"; do
		perform_groupadd "$SYSROOT" "$OPT $opts" 10
		if test "x$opts" = "x$remaining"; then
			break
		fi
		opts=`echo "$remaining" | cut -d ';' -f 1`
		remaining=`echo "$remaining" | cut -d ';' -f 2-`
	done
fi

if test "x$USERADD_PARAM" != "x"; then
	echo "Running useradd commands..."
	# Invoke multiple instances of useradd for parameter lists
	# separated by ';'
	opts=`echo "$USERADD_PARAM" | cut -d ';' -f 1`
	remaining=`echo "$USERADD_PARAM" | cut -d ';' -f 2-`
	while test "x$opts" != "x"; do
		perform_useradd "$SYSROOT" "$OPT $opts" 10
		if test "x$opts" = "x$remaining"; then
			break
		fi
		opts=`echo "$remaining" | cut -d ';' -f 1`
		remaining=`echo "$remaining" | cut -d ';' -f 2-`
	done
fi

if test "x$GROUPMEMS_PARAM" != "x"; then
	echo "Running groupmems commands..."
	# Invoke multiple instances of groupmems for parameter lists
	# separated by ';'
	opts=`echo "$GROUPMEMS_PARAM" | cut -d ';' -f 1`
	remaining=`echo "$GROUPMEMS_PARAM" | cut -d ';' -f 2-`
	while test "x$opts" != "x"; do
		perform_groupmems "$SYSROOT" "$OPT $opts" 10
		if test "x$opts" = "x$remaining"; then
			break
		fi
		opts=`echo "$remaining" | cut -d ';' -f 1`
		remaining=`echo "$remaining" | cut -d ';' -f 2-`
	done
fi


%post
# lldpd - postinst
#!/bin/sh
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-r $D"
	else
		OPT="-s"
	fi
	update-rc.d $OPT lldpd defaults
fi
if [ x"$D" = "x" ]; then
	if [ -x /sbin/ldconfig ]; then /sbin/ldconfig ; fi
fi


%preun
# lldpd - prerm
#!/bin/sh
if [ "$1" = "0" ] ; then
if [ -z "$D" ]; then
	/etc/init.d/lldpd stop
fi
fi

%postun
# lldpd - postrm
#!/bin/sh
if [ "$1" = "0" ] ; then
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-r $D"
	else
		OPT=""
	fi
	update-rc.d $OPT lldpd remove
fi
fi

%files
%defattr(-,-,-,-)
%dir "/usr"
%dir "/etc"
%dir "/usr/lib"
%dir "/usr/sbin"
"/usr/lib/liblldpctl.so.4.8.0"
"/usr/lib/liblldpctl.so.4"
"/usr/sbin/lldpctl"
"/usr/sbin/lldpcli"
"/usr/sbin/lldpd"
%dir "/etc/lldpd.d"
%dir "/etc/init.d"
%dir "/etc/default"
"/etc/lldpd.conf"
"/etc/lldpd.d/README.conf"
"/etc/init.d/lldpd"
"/etc/default/lldpd"

%files -n lldpd-zsh-completion
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/share"
%dir "/usr/share/zsh"
%dir "/usr/share/zsh/vendor-completions"
"/usr/share/zsh/vendor-completions/_lldpcli"

%files -n lldpd-dbg
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/src"
%dir "/usr/lib"
%dir "/usr/sbin"
%dir "/usr/src/debug"
%dir "/usr/src/debug/lldpd"
%dir "/usr/src/debug/lldpd/0.9.2-r0"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/compat"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/marshal.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/marshal.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lldpd-structs.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/ctl.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/log.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/log.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lldpd-structs.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/version.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/ctl.h"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/priv.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/forward-linux.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/client.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/main.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/privsep_io.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/lldpd.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/interfaces.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/pattern.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/priv-linux.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/netlink.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/lldpd.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/frame.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/interfaces-linux.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/frame.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/event.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/dmi-linux.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/sonmp.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/edp.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/lldp.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/edp.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/cdp.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/cdp.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/daemon/protocols/sonmp.h"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/errors.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/helpers.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/helpers.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/connection.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atom.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/fixedpoint.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/fixedpoint.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atom.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/lldpctl.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/dot3.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/config.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/chassis.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/mgmt.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/med.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/dot1.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/port.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/lib/atoms/interface.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/compat/empty.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/misc.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/conf-dot3.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/display.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/conf-system.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/writer.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/show.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/client.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/kv_writer.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/conf-med.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/lldpcli.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/text_writer.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/conf-power.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/commands.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/tokenizer.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/conf-lldp.c"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/src/client/conf.c"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/netinet"
%dir "/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/hdlc"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/rtnetlink.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/filter.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/if_link.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/if_packet.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/types.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/ethtool.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/wireless.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/netlink.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/if.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/if_addr.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/linux/hdlc/ioctl.h"
"/usr/src/debug/lldpd/0.9.2-r0/lldpd-0.9.2/include/netinet/if_ether.h"
%dir "/usr/lib/.debug"
"/usr/lib/.debug/liblldpctl.so.4.8.0"
%dir "/usr/sbin/.debug"
"/usr/sbin/.debug/lldpcli"
"/usr/sbin/.debug/lldpd"

%files -n lldpd-staticdev
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/lib"
"/usr/lib/liblldpctl.a"

%files -n lldpd-dev
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/lib"
%dir "/usr/include"
%dir "/usr/lib/pkgconfig"
"/usr/lib/liblldpctl.so"
"/usr/lib/liblldpctl.la"
"/usr/lib/pkgconfig/lldpctl.pc"
"/usr/include/lldp-const.h"
"/usr/include/lldpctl.h"

%files -n lldpd-doc
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/share"
%dir "/usr/share/doc"
%dir "/usr/share/man"
%dir "/usr/share/doc/lldpd"
"/usr/share/doc/lldpd/README.md"
"/usr/share/doc/lldpd/ChangeLog"
"/usr/share/doc/lldpd/CONTRIBUTE.md"
"/usr/share/doc/lldpd/NEWS"
%dir "/usr/share/man/man8"
"/usr/share/man/man8/lldpctl.8"
"/usr/share/man/man8/lldpd.8"
"/usr/share/man/man8/lldpcli.8"

%files -n lldpd-bash-completion
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/share"
%dir "/usr/share/bash-completion"
%dir "/usr/share/bash-completion/completions"
"/usr/share/bash-completion/completions/lldpcli"

